import * as flsFunctions from "./modules/functions.js";
let buttonImage = document.querySelector(".top-menu__button-sandwich-image");
let buttonList = document.querySelector(".top-menu__button-list-wrapper");
let bodyElement = document.querySelector("body");

function setButtonListener () {
    
    buttonImage.addEventListener("click", function(event) {
        buttonImage.classList.toggle("button-active");
        buttonList.classList.toggle("list-active");
        if (event.target.className == "top-menu__button-sandwich-image button-active") {
            event.target.src = "img/Top_menu/menu-button-close.png";
        } else {
            event.target.src = "img/Top_menu/menu-button-sandwich.png";
        }
    })
}

function setDocumentListener() {
    bodyElement.addEventListener("click", function(event){
        if (event.target.className !== "top-menu__button-sandwich-image button-active" ) {
            buttonImage.src = "img/Top_menu/menu-button-sandwich.png";
            buttonImage.className = "top-menu__button-sandwich-image";
            buttonList.classList.remove("list-active");
        }
    }
    );
}

setButtonListener();
setDocumentListener();
flsFunctions.isWebp();